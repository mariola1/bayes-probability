import csv


def dic_key_count(dic, key):
    if dic.get(key, None) is None or key is None:
        return 1
    else:
        return int(dic[key])


def dic_inc(dic, key):
    if dic.get(key, None) is None:
        dic[key] = 1
    else:
        dic[key] += 1


def save_csv_file_as_a_list(csv_file):
    with open(csv_file, 'r') as f:
        csv_object = csv.reader(f)
        data_in_list_format = list(csv_object)
    return data_in_list_format


def is_complete(item, position):
    return item[position] != '?'


def order_csv_data(data_in_list_format):
    heading = data_in_list_format.pop(0)
    # We assume that in last column of csv file is enquired column
    completed_data = []
    incompleted_data = []
    enquired_column = len(heading) - 1
    [completed_data.append(item) if is_complete(item, enquired_column) \
         else incompleted_data.append(item) \
     for item in data_in_list_format]
    return (heading, completed_data, incompleted_data, enquired_column)


def file_to_order_csv_file(csv_file):
    data = save_csv_file_as_a_list(csv_file)
    ordered_data = order_csv_data(data)
    return ordered_data


def count_classes(complete_data, enquired_column, heading=None):
    if heading is None:
        enquired_column_classes = {}
        [dic_inc(enquired_column_classes, element[enquired_column]) for element in complete_data]
        return enquired_column_classes
    else:
        conditional_classes = {}
        for element in complete_data:
            [dic_inc(conditional_classes, (heading[i], element[i], element[enquired_column])) \
             for i in range(0, len(heading)) \
             if i != enquired_column]
        return conditional_classes


def partial_probability(heading, enquired_column, enquired_item, \
                        incomplete_item, enquired_column_classes, conditional_classes):
    probability = float(dic_key_count(enquired_column_classes, enquired_item[0]) / len(conditional_classes))
    for i in range(0, len(heading)):
        if i != enquired_column:
            probability = probability * float(dic_key_count(conditional_classes, \
                                                            (heading[i], incomplete_item[i],
                                                             enquired_item[0])) \
                                              / dic_key_count(enquired_column_classes, enquired_item[0]))
    return probability
