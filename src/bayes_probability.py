import sys
import src.functions as fn

# The program reads CSV file and calculates bayes probability of data which is marked in CSV file as "?"
# First row of input file contains headings with columns' names of CSV file separeted by ","
# Every additiona row contains values coresponding to each column name separeted by ","
# Question marks in CSV file are only acceptable in last column
# Question mark indicates on value that should be calculated


def bayes_probability(heading, incompleted_data, enquired_column):
    completed_items = []
    enquired_column_classes = fn.count_classes(complete_data, enquired_column)
    conditional_classes = fn.count_classes(complete_data, enquired_column, heading)
    partial_probability = {}
    complete_probability = {}
    probability_sum = 0
    for incomplete_item in incompleted_data:
        for enquired_item in enquired_column_classes.items():
            probability = fn.partial_probability(heading, enquired_column, enquired_item, \
                                                 incomplete_item, enquired_column_classes, conditional_classes)
            partial_probability[enquired_item[0]] = probability
            probability_sum += probability
        for enquired_item in enquired_column_classes.items():
            complete_probability[enquired_item[0]] = float(partial_probability[enquired_item[0]] / probability_sum)
        incomplete_item[enquired_column] = complete_probability
        completed_items.append(incompleted_data)
    return completed_items


# Program start
if len(sys.argv) < 2:
    sys.exit('Please, input as an argument the name of the CSV file.')

(heading, complete_data, incomplete_data, enquired_column) = fn.file_to_order_csv_file(sys.argv[1])

# Calculate the Bayesian probability for the incomplete data
# and output it.
completed_data = bayes_probability(
    heading, incomplete_data, enquired_column)
print(completed_data)
