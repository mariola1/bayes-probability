from unittest import TestCase

from src.functions import dic_inc, is_complete, dic_key_count


class Test(TestCase):
    def test_dic_key_count_if_key_is_none(self):
        """
        Test that dic_key_count return 1 when given key is None
        """
        given_dict = {'test': '2'}
        result = dic_key_count(given_dict, 'None')
        self.assertEqual(result, 1)

    def test_dic_inc_if_value_not_in_dict(self):
        """
        Test that dic_inc set value 1 if given key in given dic is None
        """
        given_dict = {'test': '2'}
        dic_inc(given_dict, 'given_key')
        self.assertEqual(given_dict['given_key'], 1)

    def test_is_complete_with_item_contains_special_character(self):
        """
        Test that is_complete return False if contain ? character for given position
        """
        given_item = {'test': '?'}
        result = is_complete(given_item, 'test')
        self.assertFalse(result)

    def test_is_complete_with_item_not_contains_special_character(self):
        """
        Test that is_complete return True if not contain ? character for given position
        """
        given_item = {'test': '1'}
        result = is_complete(given_item, 'test')
        self.assertTrue(result)
