# Naive Bayes Classifiers
In machine learning, naïve Bayes classifiers are a family of simple "probabilistic classifiers" based on applying Bayes' theorem with strong (naïve) independence assumptions between the features.
More about Naive Bayes Classifier --> https://en.wikipedia.org/wiki/Naive_Bayes_classifier


**Naive Bayes Classifiers Implementation**

The program reads CSV file and calculates bayes probability of data which is marked in CSV file as "?". First row of input file contains headings with columns' names of CSV file separeted by ","
Every additiona row contains values coresponding to each column name separeted by ",". Question marks in CSV file are only acceptable in last column. Question mark indicates on value that should be calculated
